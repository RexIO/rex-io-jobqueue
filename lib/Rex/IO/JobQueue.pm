#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::JobQueue;

use strict;
use warnings;

our $VERSION = "0.1";

use Data::Dumper;
use Mojo::Redis;
use Mojo::Log;
use Mojo::JSON;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   if($self->config->{log}->{file} eq "-") {
      $self->{"log"} = Mojo::Log->new(level => $self->config->{log}->{level});
   }
   else {
      $self->{"log"} = Mojo::Log->new(path => $self->config->{log}->{file}, level => $self->config->{log}->{level});
   }
   $self->{json}  = Mojo::JSON->new;

   $self->log->debug("Initializing redis object (" . $self->config->{redis}->{server} . ":" . $self->config->{redis}->{port} . ")");
   $self->{redis} = Mojo::Redis->new(server => $self->config->{redis}->{server} . ":" . $self->config->{redis}->{port}, timeout => 0);

   $self->redis->timeout(0);

   $self->redis->ping(
      sub {
         my ($redis, $res) = @_;
         if (defined $res) {
            $self->log->debug("Connected to Redis: got $res");
         }
         else {
            $self->log->error("Can't connect to redis.");
            exit 1;
         }
      }
   );

   return $self;
}

sub start {
   my ($self) = @_;

   $self->log->debug("Subscribing to " . $self->config->{redis}->{queue} . " channel");
   $self->{channel} = $self->redis->subscribe($self->config->{redis}->{queue});

   $self->channel->on(
      message => sub {
         my ($sub, $message, $channel) = @_;
         if($channel eq $self->config->{redis}->{queue}) {
            eval {
               $sub->timeout(0);
            };
            $self->process_request($message);
         }
      },
   );

   $self->channel->on(
      error => sub {
         $self->log->debug("Error in redis connection. ", join(", ", @_));
      },
   );

   $self->channel->on(
      close => sub {
         $self->log->debug("Closing redis connection.");
      },
   );

   $self->redis->ioloop->start;

   # should never come here
   $self = ref($self)->new;
   $self->start;
}

sub process_request {
   my ($self, $message) = @_;
   
   $self->log->debug("Got message: $message");
   my $ref = $self->json->decode($message);

   if(ref($ref) ne "ARRAY") {
      if($ref->{cmd} eq "Answer") {
         return;
      }

      $ref = [ $ref ];
   }

   open(my $proc, "| rex-io-jobqueue bulk") or die($!);
   print $proc $self->json->encode($ref) . "\n";
   close($proc);
}

sub run_job {
   my ($self, $ref) = @_;

   my $lock_name = $ref->{host} . ":_lock";
   $self->redis->setnx($lock_name => 1, sub {
      my ($redis, $res) = @_;
      $redis->timeout(0);
      if($res == 0) {
         $self->log->debug("FOUND lock: $lock_name - doing nothing.");
         $self->redis->quit();
         return;
      }
      else {
         $self->log->debug("No lock found: ($lock_name)");

         my $cmd = $ref->{cmd};
         delete $ref->{cmd};

         my $klass = "Rex::IO::JobQueue::Command::\u$cmd";
         $self->log->debug("Loading Command Class: $klass");

         eval "use $klass";
         if($@) {
            $self->log->info("Error loading command class: $klass");
            return;
         }

         my $c = $klass->new(app => $self);
         $c->run($ref);

         my $a_ref = $ref;
         $a_ref->{cmd} = "Answer";

         $self->redis->publish($self->config->{redis}->{queue} => $self->json->encode($a_ref), sub {
            $self->remove_lock($lock_name, sub {
               $self->redis->quit();
            });
         });

      }
   });

   $self->redis->ioloop->start;
}

sub remove_lock {
   my ($self, $lock_name, $callback) = @_;

   $self->log->debug("Removing lock: $lock_name");
   $self->redis->del($lock_name, sub {
      my($redis, $res) = @_;
      if($res) {
         $self->log->debug("Removed lock: $lock_name");
      }
      else {
         $self->log->error("Removing lock: $lock_name");
      }

      if($callback) {
         &$callback();
      }
   });
}

sub redis   { (shift)->{redis};   }
sub channel { (shift)->{channel}; }
sub log     { (shift)->{"log"};   }
sub json    { (shift)->{json};    }

sub config  {
   my @cfg = ("/etc/rex/io/jobqueue.conf", "/usr/local/etc/rex/io/jobqueue.conf", "jobqueue.conf");
   my $cfg;
   for my $file (@cfg) {
      if(-f $file) {
         $cfg = $file;
         last;
      }
   }

   my $config = {};

   if($cfg && -f $cfg) {
      my $content = eval { local(@ARGV, $/) = ($cfg); <>; };
      $config  = eval 'package Rex::IO::Config::Loader;'
                           . "no warnings; $content";

      die "Couldn't load configuration file: $@" if(!$config && $@);
      die "Config file invalid. Did not return HASH reference." if( ref($config) ne "HASH" );

      return $config;
   }
   else {
      print "Can't find configuration file.\n";
      exit 1;
   }
}

1;
