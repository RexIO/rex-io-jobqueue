#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::JobQueue::Command::Execute;

use Rex::IO::JobQueue::Command::Base;
use base qw(Rex::IO::JobQueue::Command::Base);

use Cwd 'getcwd';

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = $proto->SUPER::new(@_);

   bless($self, $proto);

   return $self;
}

sub run {
   my ($self, $option) = @_;

   my $cwd = getcwd;
   my $on_host = "";
   if(exists $option->{host}) {
      $on_host = " -H " . $option->{host};
   }

   my $script_dir = $self->app->config->{"script_path"} . "/" . $option->{script};
   if(-d $script_dir) {
      chdir $script_dir;
      $self->app->log->debug("Executing (" . $option->{script} . "): rex-io-jobqueue -B $on_host " . $option->{task});
      system("rex-io-jobqueue -B $on_host " . $option->{task});
      if($? != 0) {
         $self->app->log->error("Executing rex-io-jobqueue -B $on_host " . $option->{task});
      }
      else {
         $self->app->log->info("Successfully executed " . $option->{task} . " on $on_host");
      }
   }
   else {
      $self->app->log->info("Error changing to $script_dir");
   }

   chdir $cwd;
}

1;
